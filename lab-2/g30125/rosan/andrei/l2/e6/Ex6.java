package g30125.rosan.andrei.l2.e6;

import java.util.Scanner;

public class Ex6 {

	static int factorial_recursiv(int n)
	{
		if(n==0)
			return 1;
		return n*factorial_recursiv(n-1);
	}
	
	static int factorial_nerecursiv(int n)
	{
		int f=1;
		for(int i=1;i<=n;i++)
			f*=i;
		return f;
	}
	
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		System.out.println("Introduceti un numar:");
		int n=in.nextInt();
		int a,b;
		a=factorial_recursiv(n);
		System.out.println("Rezultatul factorialului recursiv este:"+a);
		b=factorial_nerecursiv(n);
		System.out.println("Rezultatul factorialului nerecursiv este:"+b);
		in.close();
	}
}
