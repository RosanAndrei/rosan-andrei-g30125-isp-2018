package g30125.rosan.andrei.l2.e3;

import java.util.Scanner;

public class Ex3 {
	
	static int prim(int x){
	int d=2;
		while(d*d<=x)
		{
			if(x%d==0) 
				return 0;
			d++;
		}
				return 1;
	}
	
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Introduceti capatul inferior al intervalului:");
		int A=in.nextInt();
		System.out.println("Introduceti capatul superior al intervalului:");
		int B=in.nextInt();
		int i,c=0;
		for(i=A;i<=B;i++)
			if(prim(i)==1)
			{
				c++;
				System.out.println(" "+i);
			}
			System.out.println("\nNumarul de numere prime din interval este:"+c);
	in.close();
	}
		
	
}
