package g30125.rosan.andrei.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class Ex7 {
	public static void main(String[] args)
	{
		Random r = new Random();
		Scanner in = new Scanner(System.in);
		int a=r.nextInt(30);
		int k=0;
		while(k<3)
		{
			System.out.println("Care este numarul generat?: ");
			int n=in.nextInt();
			if(n==a)
			{
				System.out.println("Felicitari, ati ghicit numarul generat!");
				k=3;
			}
			else if(n<a)
			{
				System.out.println("Raspuns gresit, numarul dumneavoastra este prea mic!");
				k++;
			}
			else if(n>a)
			{
				System.out.println("Raspuns gresit, numarul dumneavoastra este prea mare!");
				k++;
			}
			if(k==3)
				System.out.println("Ai pierdut!");
		}
		in.close();	
	}

}
