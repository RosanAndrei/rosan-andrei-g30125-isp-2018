package g30125.rosan.andrei.l9.e3;

 

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class TextDisplay extends JFrame{
 
	  JTextField tField;
      JTextArea tArea;
      JButton bLoghin;
 
      TextDisplay(){
 
 
            setTitle("Test Counter");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(400,300);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 
            tField = new JTextField();
            tField.setBounds(10, 20, 80, 20);
            
            bLoghin = new JButton("Counter");
            bLoghin.setBounds(10,60,width, height);
 
            bLoghin.addActionListener(new TratareButonLoghin());
 
            tArea = new JTextArea();
            tArea.setBounds(150,50,400,200);
            
 
            add(bLoghin);
            add(tArea);
            add(tField);
 
      }
 
      public static void main(String[] args) {
            new TextDisplay();
      }
 
      class TratareButonLoghin implements ActionListener{
    	
            public void actionPerformed(ActionEvent e) {
            	String txt = TextDisplay.this.tField.getText();
            	TextDisplay.this.tArea.setText("");
            	try (BufferedReader br = new BufferedReader(new FileReader(txt))) {

        			String sCurrentLine;

        			while ((sCurrentLine = br.readLine()) != null) {
        				
        				TextDisplay.this.tArea.append(sCurrentLine+"\n");
        			}
        		
        			

        		}
            	catch (IOException y) {
            		TextDisplay.this.tArea.setText("");
            		TextDisplay.this.tArea.append("Fisierul nu exista!"+"\n");
        		}
          }
     }
}