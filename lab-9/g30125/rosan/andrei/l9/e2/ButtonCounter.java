package g30125.rosan.andrei.l9.e2;
 

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.*;

 
public class ButtonCounter extends JFrame{
 
 
      JTextArea tArea;
      JButton bLoghin;
 
      ButtonCounter(){
 
 
            setTitle("Test Counter");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(400,300);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 
 
            bLoghin = new JButton("Counter");
            bLoghin.setBounds(10,50,width, height);
 
            bLoghin.addActionListener(new TratareButonLoghin());
 
            tArea = new JTextArea();
            tArea.setBounds(150,50,80,20);
            
 
            add(bLoghin);
            add(tArea);
 
      }
 
      public static void main(String[] args) {
            new ButtonCounter();
      }
 
      class TratareButonLoghin implements ActionListener{
    	  private int counter=0;
            public void actionPerformed(ActionEvent e) {
            	ButtonCounter.this.tArea.setText("");
            	ButtonCounter.this.tArea.append("counter:"+counter);
            	counter++; 
                  
 
            }    
      }
}