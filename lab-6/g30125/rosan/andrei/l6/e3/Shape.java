package g30125.rosan.andrei.l6.e3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.*;

public interface Shape {

	public abstract void draw(Graphics g);
	public abstract Object getId();

}
