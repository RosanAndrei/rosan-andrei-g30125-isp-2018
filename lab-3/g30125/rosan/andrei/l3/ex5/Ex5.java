package g30125.rosan.andrei.l3.ex5;

import becker.robots.*;

public class Ex5 {
	public static void main(String[] args)
	   {
	      City ny = new City();
	      Wall blockAve0 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve4 = new Wall(ny, 1, 1, Direction.WEST);
	      Wall blockAve5 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve6 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Wall blockAve7 = new Wall(ny, 1, 2, Direction.SOUTH);
	      Robot mark = new Robot(ny, 1, 2, Direction.SOUTH);
	      Thing parcel = new Thing(ny, 2, 2);
	 
	 
	     
	      mark.turnLeft();  
	      mark.turnLeft(); 
	      mark.turnLeft(); 
	      mark.move();
	      mark.turnLeft(); 
	      mark.move();
	      mark.turnLeft();  
	      mark.move();
	      mark.pickThing();
	      mark.turnLeft();  
	      mark.turnLeft(); 
	      mark.move();
	      mark.turnLeft();  
	      mark.turnLeft(); 
	      mark.turnLeft();
	      mark.move();
	      mark.turnLeft();  
	      mark.turnLeft(); 
	      mark.turnLeft();
	      mark.move();
	      mark.putThing();
	      mark.turnLeft();  
	      mark.turnLeft(); 
	      mark.turnLeft();
	     
	  
	      
	   }
}
