package g30125.rosan.andrei.l3.ex6;

import java.lang.Math;

public class MyPoint {

	private int x,  y;
	int getX()
	{
		return x;
	}
	
	int getY()
	{
		return y;
	}

	public MyPoint()
	{
		this.x=0;
		this.y=0;
	}
	public MyPoint(int x1, int y1)
	{
		this.x=x1;
		this.y=y1;
	}
	public void setX(int x)
	{
		this.x=x;
	}
	public void setY(int y)
	{
		this.y=y;
	}
	public void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	public String toString(MyPoint p) {
        String result = "(" + p.getX() + "," + p.getY() +")";
        return result;
    }
	public double distance(int x, int y) 
	{
		 return Math.sqrt((this.x - x)*(this.x - x) + (this.y - y)*(this.y - y));
	}
	public double distance(MyPoint another) 
	{
		 return Math.sqrt((this.x - another.x)*(this.x - another.x) + (this.y - another.y)*(this.y - another.y));
	}
}

