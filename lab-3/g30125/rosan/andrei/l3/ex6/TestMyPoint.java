package g30125.rosan.andrei.l3.ex6;

public class TestMyPoint {
public static void main(String[] args)
{
	
	     MyPoint p1 = new MyPoint(1,1);
	     MyPoint p2 = new MyPoint(5,7);
	     System.out.println(p1.distance(p2));
	     System.out.println(p1.distance(2, 0));
	     p1.setXY(3, 10);
	     System.out.println(p1.distance(5, 6));
	     System.out.println(p1.toString(p1));
	}
}
