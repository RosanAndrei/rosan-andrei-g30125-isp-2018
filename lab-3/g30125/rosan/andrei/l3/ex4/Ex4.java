package g30125.rosan.andrei.l3.ex4;

import becker.robots.*;

public class Ex4 {
	public static void main(String[] args)
	   {
	      City ny = new City();
	      Wall blockAve0 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve1 = new Wall(ny, 2, 2, Direction.EAST);
	      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve4 = new Wall(ny, 1, 1, Direction.WEST);
	      Wall blockAve5 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve6 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Wall blockAve7 = new Wall(ny, 2, 2, Direction.SOUTH);
	      Robot mark = new Robot(ny, 0, 2, Direction.WEST);
	     
	 
	 
	      mark.move();
	      mark.move();
	      mark.turnLeft();      
	      mark.move();
	      mark.move();
	      mark.move();
	      mark.turnLeft();  
	      mark.move();
	      mark.move();
	      mark.move();
	      mark.turnLeft();  
	      mark.move();
	      mark.move();
	      mark.move();
	      mark.turnLeft();
	      mark.move();
	     
	  
	      
	   }
}