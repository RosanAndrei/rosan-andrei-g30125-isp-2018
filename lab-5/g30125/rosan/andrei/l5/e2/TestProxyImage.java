package g30125.rosan.andrei.l5.e2;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestProxyImage {
	ProxyImage i1 = new ProxyImage("image1",1);
	ProxyImage i2 = new ProxyImage("image2",0);
	
	@Test
	public void test1()
	{
		assertEquals(i1.display(),"Display rotated image1");
	}
	
	@Test
	public void test2()
	{
		assertEquals(i2.display(),"Displaying image2");
	}
	

}
