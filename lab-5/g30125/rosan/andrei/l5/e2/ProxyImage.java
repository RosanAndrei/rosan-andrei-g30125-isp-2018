package g30125.rosan.andrei.l5.e2;

public class ProxyImage implements Image
{
	 
	   private RotatedImage rotatedImage; 
	   private RealImage realImage;
	   private String fileName;
	   private int x;
	 
	   public ProxyImage(String fileName, int x)
	   {
	      this.fileName = fileName;
	      this.x = x;
	   }
	 
	   @Override
	   public String display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      if(rotatedImage == null){
		         rotatedImage = new RotatedImage(fileName);
		      }
	      if(this.x==0)
	    	  return realImage.display();
	      else 
	    	  return rotatedImage.display();
	   }
}