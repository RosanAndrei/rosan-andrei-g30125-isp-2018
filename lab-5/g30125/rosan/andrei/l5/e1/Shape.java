package g30125.rosan.andrei.l5.e1;

public abstract class Shape {
	protected String color;
	protected boolean filled;
	
	public Shape()
	{
	
	}
	
	public Shape(String c, boolean f)
	{
		this.color=c;
		this.filled=f;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public void setColor(String c)
	{
		this.color=c;
	}
	
	public boolean isFilled()
	{
		return filled;
	}
	
	public abstract double getArea();
	{
		
	}
	
	public abstract double getPerimeter();
	{
		
	}
	
	public String toString()
	{
		return "A Shape with color of "+color+" and filled="+filled;
	}
	
}
