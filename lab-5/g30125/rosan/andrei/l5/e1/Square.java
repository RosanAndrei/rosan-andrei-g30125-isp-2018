package g30125.rosan.andrei.l5.e1;

public class Square extends Rectangle{

	public Square()
	{
		
	}
	
	public Square(double s)
	{
		super(s,s);
	}
	
	public Square(double s, String c, boolean f)
	{
		super(s,s,c,f);
	}
	
	public double getSide()
	{
		return width;
	}
	
	public void setSide(double s)
	{
		super.width = s;
		super.length = s;
	}
	
	@Override
	public String toString()
	{
		return "A Square with side="+width+", which is a subclass of "+super.toString();	
	}
}
