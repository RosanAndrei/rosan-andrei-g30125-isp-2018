package g30125.rosan.andrei.l5.e1;

public class Circle extends Shape {
	protected double radius;
	
	public Circle()
	{
	}
	
	public Circle(double r)
	{
		this.radius=r;
	}
	
	public Circle(double r, String c, boolean f)
	{
		super(c,f);
		this.radius=r;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public void setRadius(double r)
	{
		this.radius=r;
	}
	
	public double getArea()
	{
		return Math.PI*this.radius*this.radius;
	}
	
	public double getPerimeter()
	{
		return 2*Math.PI*this.radius;
	}
	
	@Override
	public String toString()
	{
		return "A Circle with radius="+radius+", which is a subclass of "+super.toString();
	}
}
