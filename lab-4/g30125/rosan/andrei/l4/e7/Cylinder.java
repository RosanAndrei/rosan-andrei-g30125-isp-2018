package g30125.rosan.andrei.l4.e7;

import g30125.rosan.andrei.l4.e3.*;

public class Cylinder extends Circle {
	private double height = 1.0;
	
	public Cylinder()
	{
		super();
	}
	
	public Cylinder(double r)
	{
		super(r);
	}
	
	public Cylinder(double r, double h)
	{
		super(r);
		this.height = h;
	}
	
	public double getHeight()
	{
		return height;
	}
	
	public double getVolume()
	{
		return super.getArea()*this.height;
	}
	
	@Override
	public double getArea()
	{
		return 2*Math.PI*getRadius()*(getRadius()+this.height);
	}

}
