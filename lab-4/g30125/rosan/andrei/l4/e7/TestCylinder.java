package g30125.rosan.andrei.l4.e7;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestCylinder{

	@Test
	public void testVolume()
	{
	Cylinder c=new Cylinder(2,1);
	assertEquals(c.getVolume(),4*Math.PI,0.01);
	}

	@Test
	public void testArea() 
	{
	Cylinder c=new Cylinder(1,1);
	assertEquals(c.getArea(),4*Math.PI,0.01);
	}
}
