package g30125.rosan.andrei.l4.e8;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestShape {
		Circle c = new Circle(1.2,"green",false);
		Rectangle r = new Rectangle(1.5,3.8,"orange",true);
		Square s = new Square(4.0,"black",false);
	
	@Test
	public void test1()
	{
		assertEquals(c.toString(),"A Circle with radius=1.2, which is a subclass of A Shape with color of green and filled=false");
	}
	
	@Test
	public void test2() 
	{
		assertEquals(r.toString(),"A Rectangle with width=1.5 and length=3.8, which is a subclass of A Shape with color of orange and filled=true");
	}
	
	@Test
	public void test3() 
	{
		assertEquals(s.toString(),"A Square with side=4.0, which is a subclass of A Rectangle with width=4.0 and length=4.0, which is a subclass of A Shape with color of black and filled=false");
	}
	
}


