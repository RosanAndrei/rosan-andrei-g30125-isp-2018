package g30125.rosan.andrei.l4.e8;

public class Shape {
	private String color;
	private boolean filled; 
	
	public Shape()
	{
		this.color = "red";
		this.filled = true;
	}
	
	public Shape(String c, boolean f)
	{
		this.color = c;
		this.filled = f;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public void setColor(String c)
	{
		this.color = c;
	}
	
	public boolean isFilled()
	{
		return filled;
	}
	
	public void setFilled(boolean f)
	{
		this.filled = f;
	}
	
	public String toString()
	{
		return "A Shape with color of "+this.color+" and filled="+this.filled; 
	}
}