package g30125.rosan.andrei.l4.e3;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCircle {
	
		Circle c1 = new Circle();
		Circle c2 = new Circle(2.0);
		
		@Test
		public void testRadiusC1()
		{
			assertEquals(c1.getRadius(),1.0,0.01);
		}
		
		@Test
		public void testAreaC2()
		{
			assertEquals(c2.getArea(),Math.PI*2.0*2.0,0.01);
		}
		
		
		
}
