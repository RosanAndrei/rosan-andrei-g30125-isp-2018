package g30125.rosan.andrei.l4.e5;
import g30125.rosan.andrei.l4.e4.*;

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qtyInStock;
	
	public Book(String n, Author a, double p)
	{
		this.name = n;
		this.author = a;
		this.price = p;
	}
	
	public Book(String n, Author a, double p, int q)
	{
		this.name = n;
		this.author = a;
		this.price = p;
		this.qtyInStock = q;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Author getAuthor()
	{
		return author;
	}
	
	public double getPrice()
	{
		return price;
	}
	
	public void setPrice(double p)
	{
		this.price = p;
	}
	
	public int getQtyInStock()
	{
		return qtyInStock;
	}
	
	public void setQtyInStock(int q)
	{
		this.qtyInStock = q;
	}
	
	public String toString()
	{
		return this.name+" by author-"+author.toString();
	}
	
	
	
	

}
