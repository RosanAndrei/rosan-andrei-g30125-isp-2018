package g30125.rosan.andrei.l4.e6;
import static org.junit.Assert.*;
import org.junit.Test;
import g30125.rosan.andrei.l4.e4.*;

public class TestBook {
	
	@Test
	public void test()
	{
	Author[] a = new Author[2];
	a[0] = new Author("author1","a1@email.com",'M');
	a[1] = new Author("author2","a2@email.com",'F');
	
	Book b = new Book("book1",a,102.42);
	assertEquals(b.toString(),"book1 by 2 authors-[author1 (M) at a1@email.com, author2 (F) at a2@email.com]");
	}
	
}
