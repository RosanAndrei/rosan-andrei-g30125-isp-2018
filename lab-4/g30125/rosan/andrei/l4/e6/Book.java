package g30125.rosan.andrei.l4.e6;
import g30125.rosan.andrei.l4.e4.*;
import java.util.Arrays;
public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock;
	
	public Book(String n, Author[] a, double p)
	{
		this.name = n;
		this.authors = a;
		this.price = p;
	}
	
	public Book(String n, Author[] a, double p, int q)
	{
		this.name = n;
		this.authors = a;
		this.price = p;
		this.qtyInStock = q;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Author[] getAuthor()
	{
		return authors;
	}
	
	public double getPrice()
	{
		return price;
	}
	
	public void setPrice(double p)
	{
		this.price = p;
	}
	
	public int getQtyInStock()
	{
		return qtyInStock;
	}
	
	public void setQtyInStock(int q)
	{
		this.qtyInStock = q;
	}
	
	public String toString()
	{
		return this.name+" by "+authors.length+" authors-"+Arrays.deepToString(authors);
	}
	
	public void printAuthors()
	{
		for(int i=0;i<authors.length;i++)
			System.out.print(authors[i].getName()+" ");
	}
	
	
	
	

}
